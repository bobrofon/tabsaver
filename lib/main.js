const {components, Cc, Ci} = require("chrome");
const nsIFilePicker = components.interfaces.nsIFilePicker;
const { pathFor } = require('sdk/system');
const path = require('sdk/fs/path');
const file = require('sdk/io/file');
const tabs = require("sdk/tabs");

function listTabs() {
	tabsUrls = [];
	for (let tab of tabs) {
		tabsUrls.push(tab.url);
	}
	return tabsUrls;
}

function saveText(filename, str){
	var textWriter = file.open(filename, 'w');
	textWriter.write(str);
	textWriter.close();
}

function readText(filename){
	if(!file.exists(filename)){
		return null;
	}
	var textReader = file.open(filename, 'r');
	var str = textReader.read();
	textReader.close();
	return str;
}

function getFileName (mode) {
	var fp = components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
	fp.init(require("sdk/window/utils").getMostRecentBrowserWindow(), "Save tabs", mode);
	fp.appendFilter("Json with urls", "*.json");
	fp.appendFilters(nsIFilePicker.filterAll);

	var rv = fp.show();
	if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
		var path = fp.file.path;
		return path;
	}
	return null;
}

function savetabs () {
	var path = getFileName(nsIFilePicker.modeSave);
	var urls = JSON.stringify(listTabs());
	saveText(path, urls);
}

function loadtabs () {
	var path = getFileName(nsIFilePicker.modeOpen);
	var urls = JSON.parse(readText(path));
	for (let url of urls) {
		tabs.open(url);
	}
}

function init () {
	var menuitems = require("menuitems");

	menuitems.Menuitem({
		id: "savetabs",
		menuid: "menu_ToolsPopup",
		label: "Save tabs",
		onCommand: savetabs,
		insertbefore: "menu_pageInfo"
	});
	menuitems.Menuitem({
		id: "loadtabs",
		menuid: "menu_ToolsPopup",
		label: "Load tabs",
		onCommand: loadtabs,
		insertbefore: "menu_pageInfo"
	});
}

init();